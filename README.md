# Example wrapper scripts

This example shows you the idea behind being able to use environment variable to control your applications launch values.

For example PetClinic and other application normally are developed to run and allow you to use a .properties file or pass command line arguments, or set variables to change what the application can speak to.

In this example you'll need to create your own MySQL database and note down the user name and password.  Ideally you should create 2, one for Dev and one for Prod with different passwords and on different servers so that you have a different host name.

## Similarities

The my.cnf.template can be likend to a Java application.properties file.
Inside the mywrapper file the **mysql** command would be equivalent to the java -jar myapp.jar.

## Steps to get this working

* Create 2 MySQL or MariaDB databases
  * Note the username and password and hostname
* Change the dev.vars and prod.vars to contain the values for your connection to the 2 databases
* Set an environment variable called **env** to either dev or prod
  ```
  export env=dev
  ```
* Run ./mywrapper to connect to your databases
